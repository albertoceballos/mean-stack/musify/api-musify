import { Server } from './server';

import mongoose from 'mongoose';

import bodyParser from 'body-parser';
import { userRoutes } from './routes/userRoutes';
import { artistRoutes } from './routes/artistRoutes';
import { albumRoutes } from './routes/albumRoutes';
import { songRoutes } from './routes/songRoutes';


var server = new Server();

//configuración de body parser
server.app.use(bodyParser.urlencoded({ extended: true }));
server.app.use(bodyParser.json());

//COnfiguración accesso CORS
server.app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

//rutas
server.app.use('/user', userRoutes);
server.app.use('/artist',artistRoutes);
server.app.use('/album',albumRoutes);
server.app.use('/song',songRoutes);

//conectar la BBDD
mongoose.connect('mongodb://localhost:27017/musify', { useNewUrlParser: true, useCreateIndex: true }, err => {
    if (err) {
        console.log("Error al conectar con la BBDD", err);
    } else {
        console.log("Base de Datos conectada en el puerto 27017");
    }
});

server.start(() => {
    console.log("Servidor Express corriendo en el puerto ", server.port);
});