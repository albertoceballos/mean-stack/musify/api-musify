import {Router,Request, Response} from 'express';
import bcrypt from 'bcrypt-nodejs';
import {User} from '../models/User';
import {JWT} from '../services/JWT';
import { validateToken } from '../middlewares/validateToken';

//multipart
var multipart=require('connect-multiparty');
var md_upload=multipart({uploadDir:'./uploads/users'});

//fileSystem
import fs from 'fs';
import path from 'path';

export var userRoutes=Router();


//crear nuevo Usuario
userRoutes.post('/create',(req:Request,res:Response)=>{
    
    var user={
        role:req.body.role || 'ROLE_USER',
        name:req.body.name,
        surname:req.body.surname,
        email:req.body.email,
        password: req.body.password,
        image:req.body.image,
    };
    if(!user.name || !user.email || !user.password){
        return res.status(400).json({
            ok:false,
            message:'Rellena todos los campos obligatorios',
        });
    }

    //encriptar contraseña
    user.password=bcrypt.hashSync(user.password);

    //crear usuario
    User.create(user).then(userCreated=>{
        if(userCreated){
            res.status(200).json({
                ok:true,
                user:userCreated,
                message:'Nuevo usuario creado con éxito',
            });
        }else{
             res.status(400).json({
                ok:false,
                message:'Error al crear nuevo usuario',
            });
        }
        
    })
    .catch(err=>{
        return res.status(400).json({
            ok:false,
            err,
        })
    });
 
});

//login de usuario
userRoutes.post('/login',(req:Request,res:Response)=>{

    var email=req.body.email;
    var password=req.body.password;
    var getHash=req.body.getHash;

    if(!email || !password){
        return res.status(400).json({
            ok:false,
            message:'Faltan datos',
        });
    }

    User.findOne({email:email},(err,user:any)=>{
        if(err){
            return res.status(500).json({
                ok:false,
                message:'Error en el login',
                error:err,
            });
        }
        //si existe el email:
        if(user){
            //comparamos las contraseñas;
            var validatePassword=bcrypt.compareSync(password,user.password);
            if (validatePassword){
                //crear el token con el servicio
                var token=JWT.createToken(user);

                //devolver el token si se pide en el cuerpo
                if(getHash){
                    return res.status(200).json({
                        ok:true,
                        token
                    });
                //si no devolver solo el usuario
                }
                  return res.status(200).json({
                        ok:true,
                        message:'Login correcto',
                        user,
                    });
                
                
            }else{
                res.status(400).json({
                    ok:false,
                    message:'La contraseña no es correcta',
                });
            }
        }else{
            res.status(400).json({
                ok:false,
                message:'El usuario no existe',
            });
        }
    });

});

//actualizar usuario
userRoutes.put('/update',validateToken,(req:any,res:Response)=>{

    var idUsuario=req.user.sub;

    var user={
        role:req.body.role || 'ROLE_USER',
        name:req.body.name,
        surname:req.body.surname,
        email:req.body.email,
        image:req.body.image,
    };

    if(!user.name || !user.email){
        return res.status(400).json({
            ok:false,
            message:'Rellena todos los campos obligatorios',
        });
    }

    User.findByIdAndUpdate(idUsuario,user,{new:true},(error,userUpdated)=>{
        if(error){
            return res.status(500).json({
                ok:false,
                error,
                message:'Error al actualizar Usuario',
            });
        }
        if(userUpdated){
            res.status(200).json({
                ok:true,
                message:'Usuario actualizado con éxito',
                user:userUpdated,
            });
        }else{
            res.status(400).json({
                ok:false,
                message:'No se pudo actualizar el usuario',
            });
        }
    });

});

//subir imagen de usuario
userRoutes.post('/upload-avatar',[validateToken,md_upload],(req:any,res:Response)=>{

    var idUsuario=req.user.sub;

    if(req.files){
        var file_path:string=req.files.imagen.path;
        var fileSplit=file_path.split('\\');
        console.log(fileSplit);
        var fileName=fileSplit[2];

        var fileSplitExtension=fileName.split('\.');
        var fileExtension=fileSplitExtension[1];
        console.log(fileExtension);
        if(fileExtension=='jpg' || fileExtension=='jpeg' || fileExtension=='png' || fileExtension=='gif'){
            User.findByIdAndUpdate(idUsuario,{image:fileName},{new:true},(error,userUpdated)=>{
                if(error){
                    return res.status(500).json({
                        ok:false,
                        error,
                        message:'Error al actualizar Usuario',
                    });
                }
                if(userUpdated){
                    res.status(200).json({
                        ok:true,
                        message:'Usuario actualizado con éxito',
                        image:fileName,
                        user:userUpdated,
                    });
                }else{
                    res.status(400).json({
                        ok:false,
                        message:'No se pudo actualizar el usuario',
                    });
                }
            });
        }else{
            fs.unlink(file_path,(err)=>{
                if(err){
                    console.log(err);
                }
            });
            res.status(400).json({
                ok:false,
                message:'El formato de la imagen no es válido',
            });
        }

    }else{
        return res.status(400).json({
            ok:false,
            message:'No has subido ninguna imagen',
        });
    }


});

userRoutes.get('/get-image-user/:image',(req:any,res:Response)=>{
    var imageFile=req.params.image;
    var pathFile='./uploads/users/'+imageFile;
    fs.exists(pathFile,(exists:any)=>{
        if(exists){
            res.sendFile(path.resolve(pathFile));
        }else{
            res.status(200).json({
                message:'No existe la imagen',
            });
        }
    });
});