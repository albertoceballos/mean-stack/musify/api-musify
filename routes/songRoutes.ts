import { Request, Response, Router } from 'express';

//modelos
import { Artist } from '../models/Artist';
import { Album } from '../models/Album';
import { Song } from '../models/Song';

//middleware autenticación
import { validateToken } from '../middlewares/validateToken';

//fileSystem
import fs from 'fs';
import path from 'path';

//multipart
var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './uploads/songs' });


export var songRoutes = Router();

//Crear nueva canción
songRoutes.post('/create', validateToken, (req: Request, res: Response) => {
    var song = {
        number: req.body.number || null,
        title: req.body.title,
        duration: req.body.duration || null,
        file: null,
        album: req.body.album,
    };

    Song.create(song).then(songCreated => {
        if (songCreated) {
            res.status(200).json({
                ok: true,
                message: 'Nueva canción creada con éxito',
                song: songCreated,
            });
        } else {
            res.status(400).json({
                ok: false,
                message: 'No se pudo crear la nueva canción',
            });
        }
    })
        .catch(error => {
            res.status(500).json({
                ok: false,
                message: 'Error al crear nueva canción',
                error,
            });
        });
});

//conseguir una canción por su id
songRoutes.get('/get/:songId', validateToken, (req: Request, res: Response) => {
    var songId = req.params.songId;
    Song.findById(songId).populate({ path: 'album' }).exec((error, song) => {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de canción',
                error,
            });
        }
        if (song) {
            res.status(200).json({
                ok: true,
                song,
            });
        } else {
            res.status(400).json({
                ok: false,
                message: 'No existe la canción',
            });
        }
    });
});

//listado de canciones
songRoutes.get('/getAll/:albumId?', validateToken, (req: Request, res: Response) => {

    var albumId = req.params.albumId;

    //si no hay parametro de Id de album saca todas las canciones, sino las canciones del album del parámetro
    if (!albumId) {
        var find = Song.find({}).sort('number');
    } else {
        var find = Song.find({ album: albumId }).sort('number');
    }

    find.populate({ path: 'album', populate: { path: 'artist', model: 'Artist' } }).exec((error, songs) => {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de álbums',
                error,
            });
        }
        if (songs) {
            res.status(200).json({
                ok: true,
                songs,
            });
        } else {
            res.status(400).json({
                ok: false,
                message: 'No hay canciones',
            });
        }
    });
});

//actualizar canciones
songRoutes.put('/update/:songId', validateToken, (req: Request, res: Response) => {
    var songId = req.params.songId;
    var updateData = req.body;

    Song.findByIdAndUpdate(songId, updateData, { new: true }, (error, songUpdated) => {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error al actualizar canción',
                error,
            });
        }
        if (songUpdated) {
            res.status(200).json({
                ok: true,
                message: 'Canción actualizada con éxito',
                song: songUpdated,
            });
        } else {
            res.status(400).json({
                ok: false,
                message: 'No se pudo actualizar la canción',
            });
        }
    });
});

//eliminar una canción
songRoutes.delete('/delete/:songId', validateToken, (req: Request, res: Response) => {
    var songId = req.params.songId;
    Song.findByIdAndDelete(songId, (error, songDeleted) => {
        if (error) {
            return res.status(500).json({
                ok: false,
                message: 'Error en la petición de borrar canción',
                error,
            });
        }
        if (songDeleted) {
            res.status(200).json({
                ok: true,
                message: 'Canción borrada con éxito',
                song: songDeleted,
            });
        } else {
            res.status(400).json({
                ok: true,
                message: 'No se pudo borrar la canción',
            });
        }
    });

});

//subir archivo de audio de la canción
songRoutes.post('/upload-file/:songId', [validateToken, md_upload], (req: any, res: Response) => {
    var songId = req.params.songId;
    if (req.files) {
        var file_path: string = req.files.file.path;
        var file_path_split = file_path.split('\\');
        var file_name = file_path_split[2];
        var file_name_split = file_name.split('\.');
        var file_extension = file_name_split[1];
        if (file_extension == 'mp3' || file_extension == 'ogg') {
            Song.findByIdAndUpdate(songId, { file: file_name }, (error, songUpdated) => {
                if (error) {
                    return res.status(500).json({
                        ok: false,
                        message: 'Error en la petición al actualizar la canción',
                        error,
                    });
                }
                if (songUpdated) {
                    res.status(200).json({
                        ok: true,
                        message: 'Archivo de audio subido con éxito',
                        song: songUpdated,
                    });
                } else {
                    res.status(400).json({
                        ok: false,
                        message: 'No se pudo actualizar la canción',
                    });
                }
            });
        } else {
            fs.unlink(file_path, (err) => {
                if (err) {
                    console.log(err);
                }
            });
            res.status(400).json({
                ok: false,
                message: 'El formato del archivo no es válido',
            });
        }
    } else {
        res.status(400).json({
            ok: false,
            message: 'No has subido ningún archivo',
        });
    }

});

//conseguir archivo de audio
songRoutes.get('/get-file/:songFile', (req: Request, res: Response) => {
    var songFile = req.params.songFile;
    if (songFile) {
        var filePath = './uploads/songs/' + songFile;
        fs.exists(filePath, (exists) => {
            if (exists) {
                return res.status(200).sendFile(path.resolve(filePath));
            } else {
                res.status(400).json({
                    ok: false,
                    message: 'No existe el archivo de audio de la canción',
                });
            }
        });
    } else {
        res.status(400).json({
            ok: false,
            message: 'No existe la canción',
        });
    }
});
