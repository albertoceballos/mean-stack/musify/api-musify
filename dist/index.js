"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var server_1 = require("./server");
var mongoose_1 = __importDefault(require("mongoose"));
var body_parser_1 = __importDefault(require("body-parser"));
var userRoutes_1 = require("./routes/userRoutes");
var artistRoutes_1 = require("./routes/artistRoutes");
var albumRoutes_1 = require("./routes/albumRoutes");
var songRoutes_1 = require("./routes/songRoutes");
var server = new server_1.Server();
//configuración de body parser
server.app.use(body_parser_1.default.urlencoded({ extended: true }));
server.app.use(body_parser_1.default.json());
//COnfiguración accesso CORS
server.app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
//rutas
server.app.use('/user', userRoutes_1.userRoutes);
server.app.use('/artist', artistRoutes_1.artistRoutes);
server.app.use('/album', albumRoutes_1.albumRoutes);
server.app.use('/song', songRoutes_1.songRoutes);
//conectar la BBDD
mongoose_1.default.connect('mongodb://localhost:27017/musify', { useNewUrlParser: true, useCreateIndex: true }, function (err) {
    if (err) {
        console.log("Error al conectar con la BBDD", err);
    }
    else {
        console.log("Base de Datos conectada en el puerto 27017");
    }
});
server.start(function () {
    console.log("Servidor Express corriendo en el puerto ", server.port);
});
