"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var songSchema = new mongoose_1.Schema({
    number: {
        type: Number,
    },
    title: {
        type: String,
        required: [true, 'El título de la canción es obligatorio'],
    },
    duration: {
        type: String,
    },
    file: {
        type: String,
    },
    album: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Album',
        required: [true, 'Debe existir una referencia a un álbum'],
    }
});
exports.Song = mongoose_1.model('Song', songSchema);
