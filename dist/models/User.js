"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var userSchema = new mongoose_1.Schema({
    role: {
        type: String,
        default: 'ROLE_USER',
    },
    name: {
        type: String,
        required: [true, 'El nombre de usuario es obligatorio'],
    },
    surname: {
        type: String,
    },
    email: {
        type: String,
        required: [true, 'El email de usuario es obligatorio'],
        unique: true,
    },
    password: {
        type: String,
        required: [true, 'La contraseña es obligatoria'],
    },
    image: {
        type: String,
        default: 'default_avatar.jpg',
    },
});
exports.User = mongoose_1.model('User', userSchema);
