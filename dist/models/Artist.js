"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var ts_mongoose_pagination_1 = require("ts-mongoose-pagination");
var artistSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: [true, 'El nombre de artista es requerido'],
    },
    description: {
        type: String,
    },
    image: {
        type: String,
        default: 'default_artist.jpg',
    }
});
artistSchema.plugin(ts_mongoose_pagination_1.mongoosePagination);
exports.Artist = mongoose_1.model('Artist', artistSchema);
