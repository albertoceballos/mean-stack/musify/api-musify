"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jwt_simple_1 = __importDefault(require("jwt-simple"));
var moment_1 = __importDefault(require("moment"));
exports.validateToken = function (req, res, next) {
    var secret = 'Una-clave-ultrasecreta';
    if (!req.headers.authorization) {
        return res.status(400).json({
            ok: false,
            message: 'La petición no tiene cabecera de autenticación',
        });
    }
    var token = req.headers.authorization;
    try {
        var payload = jwt_simple_1.default.decode(token, secret);
        if (payload.exp <= moment_1.default().unix()) {
            return res.status(400).json({
                ok: false,
                message: 'El token ha expirado',
            });
        }
    }
    catch (error) {
        return res.status(400).json({
            ok: false,
            message: 'El token NO es válido'
        });
    }
    req.user = payload;
    next();
};
