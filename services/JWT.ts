import jwt from 'jwt-simple';
import moment from 'moment';

export class JWT{
    private static semilla:string="Una-clave-ultrasecreta";

    constructor(){

    }
    
    static createToken(user:any){

        var payload={
            sub:user._id,
            name:user.name,
            surname:user.surname,
            email:user.email,
            role:user.role,
            image:user.image,
            iat:moment().unix(),
            exp:moment().add(30,'days').unix,
        };

        return jwt.encode(payload,this.semilla);
    }
}