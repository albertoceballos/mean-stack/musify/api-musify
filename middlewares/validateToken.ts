import {Request, Response, NextFunction} from 'express';
import jwt from 'jwt-simple';
import moment from 'moment';


export var validateToken=function(req:any,res:Response,next:NextFunction){
    var secret='Una-clave-ultrasecreta';
    if(!req.headers.authorization){
        return res.status(400).json({
            ok:false,
            message:'La petición no tiene cabecera de autenticación',
        });
    }
        var token:any=req.headers.authorization;

        try {
            var payload=jwt.decode(token,secret);
            if(payload.exp<=moment().unix()){
                return res.status(400).json({
                    ok:false,
                    message:'El token ha expirado',
                });
            }
            
        } catch (error) {
            return res.status(400).json({
                ok:false,
                message:'El token NO es válido'
            });
        }

        req.user=payload;
        next();
    
}
