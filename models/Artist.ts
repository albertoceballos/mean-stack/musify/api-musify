import {Schema,model,PaginateModel} from 'mongoose';
import { mongoosePagination } from "ts-mongoose-pagination";

const artistSchema=new Schema({
    name:{
        type:String,
        required:[true, 'El nombre de artista es requerido'],
    },
    description:{
        type:String,
    },
    image:{
        type:String,
        default:'default_artist.jpg',
    }
});

artistSchema.plugin(mongoosePagination);
export const Artist:PaginateModel<any>=model('Artist',artistSchema);