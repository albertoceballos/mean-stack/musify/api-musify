import {Schema,model} from 'mongoose';

var userSchema=new Schema({
    role:{
        type:String,
        default:'ROLE_USER',
    },
    name:{
        type:String,
        required:[true,'El nombre de usuario es obligatorio'],
    },
    surname:{
        type:String,
    },
    email:{
        type:String,
        required:[true,'El email de usuario es obligatorio'],
        unique:true,
    },
    password:{
        type:String,
        required:[true,'La contraseña es obligatoria'],

    },
    image:{
        type:String,
        default:'default_avatar.jpg',
    },
});


export var User=model('User',userSchema);