import {Schema,model} from 'mongoose';

var songSchema=new Schema({
    number:{
        type:Number,
    },
    title:{
        type:String,
        required:[true,'El título de la canción es obligatorio'],
    },
    duration:{
        type:String,
    },
    file:{
        type:String,
    },
    album:{
        type:Schema.Types.ObjectId,
        ref:'Album',
        required:[true,'Debe existir una referencia a un álbum'],
    }
});

export var Song=model('Song',songSchema);